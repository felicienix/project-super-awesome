#ifndef SOLVER_H
#define SOLVER_H
#include <armadillo>

struct psi_mat {
    psi_mat()
        :rpart(), ipart(){}
    psi_mat(const arma::mat& _rpart, const arma::mat& _ipart)
        :rpart(_rpart), ipart(_ipart){}
    arma::mat rpart;
    arma::mat ipart;
};

class Solver
{
public:
    arma::mat V0;
    psi_mat psi;
    double h_bar = 1.0;
    double m = 1.0;
    char method;
    double dx,dy,dt;
    arma::mat tmpr;
    arma::mat tmpi;
    arma::mat g_rpart,g_ipart,next_g_rpart,next_g_ipart;

    //Initialiser
    Solver(const arma::mat& _V0, psi_mat& psi0, double _h_bar, double _m, 
    char _method, double _dx, double _dy, double _dt);
    
    // Calcule psi(t+dt) à partir de psi, en utilisant la methode 'method'
    psi_mat calcul_psi_t_plus_dt();

    void psi_dt_FTCS();
    void psi_dt_BTCS();
    void psi_dt_CTCS();

    //void clean();
    psi_mat add_padding(psi_mat psi0);
    arma::mat padded_matrix(arma::mat m);

    double norm();

};
#endif //SOLVER_H