# Packages needed

This project uses python and so it is required that you have python 3 installed on your system.
```sh
#on debian based systems
sudo apt install -y python3
#on redhat based systems
sudo yum install -y python3
```
This project also uses the boost library as well as swig and pip packages, so you need to have them in your system too.
```sh
#on debian based systems
sudo apt-get install -y libboost-all-dev
sudo apt install -y swig
sudo apt install -y python3-pip
#on redhat based systems
sudo yum install -y boost-devel
sudo yum install -y swig
sudo yum install -y epel-release
sudo yum update
sudo yum install -y python3-pip
```

Next you will have to install several modules, using pip, that are used by this project.
```sh
pip install numpy pymongo pillow scipy colorama pyevtk matplotlib
```

And you are good to go !
